<!DOCTYPE html>
<?php
require_once('config/config.php');
require_once 'classes/phpmailer/mail.php';
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
$mysqli = new mysqli($hostname, $username, $password, $dbname); /*opening a database connection*/
if ($mysqli->connect_error) {  /*checking for error in opening connection*/
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}
if($_POST["email"]!=NULL) {
	$_SESSION["email"] = $_POST["email"];
	$_POST["email"] = NULL;
}
if($_GET["email"]!=NULL and $_GET["validation"]!=NULL and !isset($_SESSION["change"])) {
	$_SESSION["email"] = $_GET["email"];
	$sql = "SELECT * FROM user WHERE email = '".$_GET["email"]."' AND validation = '".$_GET["validation"]."'";
	$result = mysqli_query($mysqli, $sql);
	if(mysqli_num_rows($result)==1) {
		$_SESSION["change"] = 1;
	}
	else {
		unset($_SESSION["change"]);
	}
}
if((isset($_SESSION["change"])) and $_POST["new_password"]!=NULL) {
	$new_password = $_POST["new_password"];
	$sql = "UPDATE user SET password = '".$new_password."' WHERE email = '".$_SESSION["email"]."'";
	mysqli_query($mysqli,$sql);
	$_SESSION["change_message"] = "Password has been changed";
	unset($_SESSION["email"]);
	unset($_SESSION["change"]);
} 
?>
<html lang="en">
<head>
  <title>Barbero</title>
  <link rel="shortcut icon" href="favicon.ico?v=2" type="image/x-icon" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
<script language='javascript' type='text/javascript'>
    function check(input) {
        if (input.value != document.getElementById('new_password').value) {
            input.setCustomValidity('Password Must be Matching.');
        } else {
            // input is valid -- reset the error message
            input.setCustomValidity('');
        }
    }
</script>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php" style="background:rgba(0,0,0,0.1);color:white;margin-top:3px;padding-bottom:50px;border-radius:15px;"><img src = "images/logogogo edit.png" class = "img-responsive" style = "width:190px;height:auto;"/></a>
    </div>
    <div class="collapse navbar-collapse"  style = "padding-top:8px;" id="myNavbar">
      <ul class="nav navbar-nav" style = "padding-left:20px;">
        <li class="active"><a href="index.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Home</a></li>
        <li><a href="SUPPORT.html" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;" target = "_blank">Support</a></li>
		</ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log out</a></li>';
		}
		?>
        <li><a href="BARBER.html" target = "_blank" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">For Barbers</a></li>
      </ul>
    </div>
  </div>
</nav><br><br>
<div class = "container">
	<div class = "row text-center" style = "color:white;">
		<div class = "col-sm-4">
		</div>
		<?php
			if(!isset($_SESSION["change"]) and !isset($_SESSION["email"]) and $_POST["new_password"]==NULL) {
				echo '<div class = "col-sm-4" style = "background:rgba(0,0,0,0.7);padding:20px 20px 20px 20px;border-radius:15px;">
				<form action = "changepasswordweb.php" method = "POST">
						<input type = "email" name = "email" id = "email" placeholder = "Enter your email" class = "form-control"><br>
						<input type = "submit" name = "submit" value = "submit" placeholder = "Send Reset Link" class = "form-control">
					  </form></div>';
			}
			if(!isset($_SESSION["change"]) and isset($_SESSION["email"])) {
			$validation = date("y-m-d h:i:sa");
			$validation = str_replace(" ","",$validation);
			$validation = str_replace("-","",$validation);
			$validation = str_replace(":","",$validation);
			$validation = str_replace("pm","",$validation);
			$validation = str_replace("am","",$validation);
			$sql = "UPDATE user SET validation = '".$validation."' WHERE email = '".$_SESSION["email"]."'";
			if (mysqli_query($mysqli, $sql)) {
						$mail1 = new PHPMailer(); // create a new object
						$mail1->IsSMTP(); // enable SMTP
						$mail1->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
						$mail1->SMTPAuth = true; // authentication enabled
						 // secure transfer enabled REQUIRED for Gmail
						$mail1->Host = $SMTPhost;
						$mail1->Port = 465; // or 587
						$mail1->IsHTML(true);
						$mail1->Username = $SMTPusername;
						$mail1->Password = $SMTPpassword;
						$mail1->SetFrom($SMTPfrom);
						$mail1->SMTPSecure = 'ssl';
						$mail1->Subject = "Password Reset | Barbero";
						$mail1->Body = "<p>You have requested for a password change. Please follow this link to change your password -></p><p><a href = '".$url."/changepasswordweb.php?email=".$_SESSION["email"]."&validation=".$validation."'>Reset Password</a></p>";
						$mail1->AddAddress($_SESSION["email"]);
						$mail1->SmtpClose();
						$mail1->Send();
					} else {
						echo "Error: " . $sql . "<br>" . mysqli_error($mysqli);
					}
		echo '<div class = "col-sm-4"  style = "background:rgba(0,0,0,0.7);padding:20px 20px 20px 20px;border-radius:15px;">
			<h3>Change Password</h3><hr>
			<h4> A link for resetting your password has been sent to your email.</h4>
		</div>';
		} else if(isset($_SESSION["change"]) and $_POST["new_password"]==NULL) {
			echo '<div class = "col-sm-4"  style = "background:rgba(0,0,0,0.7);padding:20px 20px 20px 20px;border-radius:15px;">
					<form action = "changepasswordweb.php" method = "POST">
						<input type = "password" name = "new_password" id = "new_password" placeholder = "Enter new password" class = "form-control">
						<input type = "password" name = "confirm_new_password" id = "confirm_new_password" placeholder = "Confirm new password" oninput="check(this)" class = "form-control">
						<input type = "submit" name = "submit" id = "submit" value = "Change Password" class = "form-control">
					</form>
				  </div>';
		}
         else if($_POST["new_password"]!=NULL) {
			 echo '<div class = "col-sm-4"  style = "background:rgba(0,0,0,0.7);padding:20px 20px 20px 20px;border-radius:15px;"><h4>'.$_SESSION["change_message"].'</h4></div>';
		 }		
		?>
		<div class = "col-sm-4">
		</div>
</div>
</div>
</body>
</html>