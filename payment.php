<!DOCTYPE html>
<?php
require_once('config/config.php');
if(!isset($_SESSION['user_id'])) {
	$page = $_SERVER["REQUEST_URI"];
	$_SESSION['page'] = $page;
	$_SESSION["redirect_url"] = "http://".$_SERVER['SERVER_NAME'].$_SESSION['page'];
	header('Location: '.'login.php');
}
?>
<html lang="en">
<head>
  <title>Barbero</title>
      <link rel="SHORTCUT ICON" href="images/sssssshhh.png">

      <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
         #bgcolor {
	background: #efefef;
}
#uberbar {
		position: fixed;
		background: white;
		width: 100%;
		z-index: 99;
}
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
   <script type="text/javascript">

   //Created / Generates the captcha function    
    function DrawCaptcha()
    {
        var a = Math.ceil(Math.random() * 10)+ '';
        var b = Math.ceil(Math.random() * 10)+ '';       
        var c = Math.ceil(Math.random() * 10)+ '';  
        var d = Math.ceil(Math.random() * 10)+ '';  
        var e = Math.ceil(Math.random() * 10)+ '';  
        var f = Math.ceil(Math.random() * 10)+ '';  
        var g = Math.ceil(Math.random() * 10)+ '';  
        var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f + ' ' + g;
        document.getElementById("txtCaptcha").value = code
    }

    // Validate the Entered input aganist the generated security code function   
    function ValidCaptcha(){
        var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
        var str2 = removeSpaces(document.getElementById('txtInput').value);
        if (str1 == str2) return true;        
        var errorInput = document.getElementById("txtInput");
		errorInput.setAttribute("value","Invalid Captcha. Try again.");
		return false;
        
    }

    // Remove the spaces from the entered and generated code
    function removeSpaces(string)
    {
        return string.split(' ').join('');
    }
    
 
    </script>
</head>
<body onload="DrawCaptcha();">
<div id="bgcolor">
<div id="uberbar">
<nav class="navbar">

  <div class="container-fluid">

    <div class="navbar-header">

      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>

      <a class="navbar-brand" href="index.php" style="background:white;color:white;padding-bottom:80px;border-radius:15px;"><img src = "images/BARBEROx.png" class = "img-responsive" style = "width:300px;height:300;"/></a>
    </div>
    <div class="collapse navbar-collapse" style = "padding-top:25px;" id="myNavbar"><h5 style="font-size:100%">
      <ul class="nav navbar-nav navbar-right" style = "padding-left:2px;">
    <li><a href="SUPPORT.html" style="color:black;background:;border-style:;border-width:1px;" target = "_blank">Support</a></li>
		</ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:black;background:;border-style:;border-width:1px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:black;background:;border-style:;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:black;background:);border-style:;border-width:1px;">Logged in as '.$_SESSION['firstname'].'</a></li>';
		echo '<li><a href="logout.php" style="color:black;background:;border-style:;border-width:1px;">Log out</a></li>';
		}
		?>
            </ul><ul class="nav navbar-nav navbar-right" style = "padding-left:2px;">
        <li><a href="BARBER.html" style="color:black;background:white;border-style:;border-width:3px;">For Barbers</a></li>
       
</ul><ul class="nav navbar-nav navbar-right" style = "padding-left:2px;"> <li><a href="index.php" style="color:black;background:;border-style:;border-width:3px;">Home</a></li>
</ul>
          </b>
    </div>
  </div>
          </nav></div>
<br><br><br><br><br><br><br>
<div class = "container text-center" style = "background:white;border-radius:2px;box-shadow:2px 2px 5px 10px #e6e6e6;"><br>
      <h4 style = "color:black;">Location ------ Barbershop ------ Barbers ------ services ------  Timeslot ------ <b><i>Payment</b></i></h4>
	<br></div>
<br><br>
<br><br>
<div class = "container">
<div class = "col-sm-6 col-lg-6 col-md-6">
<div class = "row" style = "background:white;border-radius:15px;padding:20px 20px 20px 20px;box-shadow:2px 2px 5px 10px #e6e6e6;">
	<h3 style = "color:black;">Choose a payment option and make payment</h3><br>
	<h4 style = "color:black;">Make Payment at Shop:</h4>
	<form action = "generate_receipt.php" method = "POST" onsubmit="return ValidCaptcha();">
	<input type="text" id="txtCaptcha" class = "form-control" style="background-image:url('images/cap.jpg');text-align:center; border:none; font-weight:bold;width:200px; font-family:Modern">
    <input type="button" id="btnrefresh" value="Refresh" onclick="DrawCaptcha();" style = "width:200px;" class = "form-control"><br><br>
	<input type="text" id="txtInput" placeholder = "Enter Captcha" style = "width:200px;" class = "form-control"> 
	<input type = "submit" value = "Generate Receipt" class = "form-control">
	</form>
</div>
</div>
<div class = "col-sm-6 col-lg-6 col-md-6">
<div class = "row" style = "background:white;border-radius:15px;padding:20px 20px 20px 20px;color:black;box-shadow:2px 2px 5px 10px #e6e6e6;">
	<h3>Your order details</h3><br>
	<?php
		echo '<h4>Shop: '.$_SESSION["shopname"].'</h4><h5>Address: '.$_SESSION["addressline1"].'</h5><h5>&nbsp;'.$_SESSION["addressline2"].'</h5><h5> Contact: '.$_SESSION["shopcontact"].'</h5><hr>
		<h4>Barber: '.$_SESSION["barbername"].'</h4><hr><h4>Services:';
		for($i=0;$i<$_SESSION["no_of_services"];$i++) {
			echo '<h5>'.$_SESSION["typearray"][$i].'</h5><h5>Rs. '.$_SESSION["pricearray"][$i].' | Duration '.$_SESSION["durationarray"][$i].' minutes</h5><hr>';			
		
		}
		echo '<h4><div style = "float:left;">Total price (Rs.)</div><div style = "float:right;">'.$_SESSION["total_price"].'</div></h4><br>';
		echo '<h4><div style = "float:left;">Duration (minutes)</div><div style = "float:right;">'.$_SESSION["total_time"].'</div></h4><br>';
		echo '<h4><div style = "float:left;">Start time</div><div style = "float:right;">'.$_SESSION["time_from"]->format('d M Y h:i:s a').'</div></h4><br>';
		echo '<h4><div style = "float:left;">End time</div><div style = "float:right;">'.$_SESSION["time_to"]->format('d M Y h:i:s a').'</div></h4><br>';
	?>	
</div>
</div>
</div>
<br><br><br><br><br><br>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
      
  <div class = "row">
  <div class = "col-sm-1">
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.facebook.com/Barbero-433175553731182/"><img src = "images/facebook_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.instagram.com/barberoin/"><img src = "images/instagram_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.linkedin.com/company-beta/13343377"><img src = "images/linkedin_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://plus.google.com/u/2/111476464313954727781"><img src = "images/google_plus_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://twitter.com/Barberoindia"><img src = "images/twitter.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-3">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;"><b>
  <center>
		Copyright © 2017 BARBERO - All Rights Reserved.
        </center></b>
      </div>
      </footer></div>
</body>
</html>