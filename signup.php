<!DOCTYPE html>
<?php
require_once('config/config.php');
require_once 'classes/phpmailer/mail.php';
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
$mysqli = new mysqli($hostname, $username, $password, $dbname); /*opening a database connection*/
if ($mysqli->connect_error) {  /*checking for error in opening connection*/
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
}
?>
<html lang="en">
<head>
  <title>Barbero</title>
       <LINK REL="SHORTCUT ICON"
       HREF="images/sssssshhh.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
          #bgcolor {
	background: #efefef;
}
	#uberbar {
		position: fixed;
		background: white;
		width: 100%;
		z-index: 99;
}
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
<script language='javascript' type='text/javascript'>
    function check(input) {
        if (input.value != document.getElementById('password').value) {
            input.setCustomValidity('Password Must be Matching.');
        } else {
            // input is valid -- reset the error message
            input.setCustomValidity('');
        }
    }
</script>
<script type = "text/javascript">
$(document).ready(function() {
    $("#mobile").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
</script>
</head>
<body>

<div id="bgcolor">
<div id="uberbar">
<nav class="navbar">

  <div class="container-fluid">

    <div class="navbar-header">

      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>

      <a class="navbar-brand" href="index.php" style="background:white;color:white;padding-bottom:80px;border-radius:15px;"><img src = "images/new new loggozzzzzcdcdscsd.png" class = "img-responsive" style = "width:300px;height:300;"/></a>
    </div>
    <div class="collapse navbar-collapse" style = "padding-top:25px;" id="myNavbar"><h5 style="font-size:100%">
      <ul class="nav navbar-nav navbar-right" style = "padding-left:2px;">
    <li><a href="SUPPORT.html" style="color:black;background:;border-style:;border-width:1px;" target = "_blank">Support</a></li>
		</ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:black;background:;border-style:;border-width:1px;">Log in</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:black;background:);border-style:;border-width:1px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:black;background:;border-style:;border-width:1px;">Log out</a></li>';
		}
		?>
            </ul><ul class="nav navbar-nav navbar-right" style = "padding-left:2px;">
        <li><a href="BARBER.html" style="color:black;background:white;border-style:;border-width:3px;">For Barbers</a></li>
       
</ul><ul class="nav navbar-nav navbar-right" style = "padding-left:2px;"> <li><a href="index.php" style="color:black;background:#efefef;border-style:;border-width:3px;">Home</a></li>
          </ul></b>
       
    </div>
  </div>
          </nav></div>
<br><br><br><br><br><br>
<div class = "container">
	<div class = "row text-center" style = "color:black;">
		<div class = "col-sm-4">
		</div>
		<div class = "col-sm-4" style = "background:white;padding:20px 20px 20px 20px;border-radius:15px;">
		<?php 
			if(!isset($_POST['firstname'])) {
		echo '<h3>SIGN UP</h3><hr>
		<form action = "signup.php" method = "post">
			<input type = "text" id = "firstname" name = "firstname" class = "form-control" placeholder = "First Name" required><br>
			<input type = "text" id = "lastname" name = "lastname" class = "form-control" placeholder = "Last Name" required><br>
			<input type = "email" id = "email" name = "email" class = "form-control" placeholder = "Email" required><br>
			<input type = "password" id = "password" name = "password" class = "form-control" placeholder = "Password" required><br>
			<input type = "password" id = "confirm_password" name = "confirm_password" class = "form-control" placeholder = "Confirm Password" oninput="check(this)" required><br>
			<input type = "tel" maxlength = "12" minlength = "10" id = "mobile" name = "mobile" class = "form-control" placeholder = "Enter 10 digit Mobile Number (Optional)"><br>
			<input type = "submit" id = "submit" name = "submit" value = "Sign Up" class = "form-control">
		</form>';
			} else if($_POST['firstname']!= NULL) {
				$sql="SELECT * from user WHERE email = '".$_POST["email"]."'";
				$result=mysqli_query($mysqli,$sql);
				if(mysqli_num_rows($result)>0) {
					echo '<h3>This email has already been taken</h3><br><br><br><br><br><br><br><br><br>';
				}
				else {
					$validation = date("y-m-d h:i:sa");
					$validation = str_replace(" ","",$validation);
					$validation = str_replace("-","",$validation);
					$validation = str_replace(":","",$validation);
					$validation = str_replace("pm","",$validation);
					$validation = str_replace("am","",$validation);
					$sql="INSERT INTO user (firstname,lastname,email,password,mobile,validation) VALUES ('".$_POST["firstname"]."','".$_POST["lastname"]."','".$_POST["email"]."','".$_POST["password"]."','".$_POST["mobile"]."','".$validation."')";
					if (mysqli_query($mysqli, $sql)) {
						$mail1 = new PHPMailer(); // create a new object
						$mail1->IsSMTP(); // enable SMTP
						$mail1->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
						$mail1->SMTPAuth = true; // authentication enabled
						 // secure transfer enabled REQUIRED for Gmail
						$mail1->Host = $SMTPhost;
						$mail1->Port = 465; // or 587
						$mail1->IsHTML(true);
						$mail1->Username = $SMTPusername;
						$mail1->Password = $SMTPpassword;
						$mail1->SetFrom($SMTPfrom);
						$mail1->SMTPSecure = 'ssl';
						$mail1->Subject = "New Account Created On Barbero";
						$mail1->Body = "<p>Thank you signing up at Barbero!</p><p>Please validate your account by following this link -></p><p><a href = '".$url."/validation.php?email=".$_POST["email"]."&validation=".$validation."'>Validate Account</a></p>";
						$mail1->AddAddress($_POST["email"]);
						$mail1->SmtpClose();
						$mail1->Send();
						echo "<h3>Thank you for signing up!</h3><br>
							  <h4>We have sent you an email containing a validation link.</h4>
							  <h4>After you validate your account you will be able to use it</h4><br><br><br><br><br><br>";
					} else {
						echo "Error: " . $sql . "<br>" . mysqli_error($mysqli);
					}
				}
			}
		?>
		</div>
		<div class = "col-sm-4">
		</div>
	</div>
</div>
<br><br><br><br><br><br><br>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-1778657632883902",
    enable_page_level_ads: true
  });
</script>


<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
  <div class = "row">
  <div class = "col-sm-1">
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.facebook.com/Barbero-433175553731182/"><img src = "images/facebook_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.instagram.com/barberoin/"><img src = "images/instagram_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.linkedin.com/company-beta/13343377"><img src = "images/linkedin_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://plus.google.com/u/2/111476464313954727781"><img src = "images/google_plus_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://twitter.com/Barberoindia"><img src = "images/twitter.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-3">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
        <b>Copyright © 2017 Rock Paper - All Rights Reserved.</b>
  </center>
  </div>
      </footer></div>
</body>
</html>