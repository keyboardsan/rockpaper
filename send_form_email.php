<?php
require_once('config/config.php');
require_once('classes/phpmailer/mail.php');

if(isset($_POST['email'])) {
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "rockpaperindia@gmail.com";
    $email_subject = "BARBER INFORMATION";

    function died($error) {
        // your error code can go here
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }
 

    // validation expected data exists
    if(!isset($_POST['NAME']) ||
        !isset($_POST['SHOP_NAME']) ||
        !isset($_POST['email']) ||
        !isset($_POST['PHONE_NUMBER']) ||
        !isset($_POST['CITY']) ||
      !isset($_POST['STATE'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
 
     
 
    $NAME = $_POST['NAME']; // required
    $SHOP_NAME = $_POST['SHOP_NAME']; // required
    $email = $_POST['email']; // required
    $PHONE_NUMBER = $_POST['PHONE_NUMBER']; // not required
    $CITY = $_POST['CITY']; // required
    $STATE = $_POST['STATE']; // required


    $error_message = "";
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$NAME)) {
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }
 
 
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
   
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
     
 
    $email_message .= " Name: ".clean_string($NAME)."<br>";
    $email_message .= "Shop Name: ".clean_string($SHOP_NAME)."<br>";
    $email_message .= "Email: ".clean_string($email)."\n";
    $email_message .= "Phone Number: ".clean_string($PHONE_NUMBER)."<br>";
    $email_message .= "City: ".clean_string($CITY)."<br>";
    $email_message .= "State: ".clean_string($STATE)."<br>";

 
						$mail1 = new PHPMailer(); // create a new object
						$mail1->IsSMTP(); // enable SMTP
						$mail1->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
						$mail1->SMTPAuth = true; // authentication enabled
						 // secure transfer enabled REQUIRED for Gmail
						$mail1->Host = $SMTPhost;
						$mail1->Port = 465; // or 587
						$mail1->IsHTML(true);
						$mail1->Username = $SMTPusername;
						$mail1->Password = $SMTPpassword;
						$mail1->SetFrom($SMTPfrom);
						$mail1->SMTPSecure = 'ssl';
						$mail1->Subject = "Customer Contact | Barbero";
						$mail1->Body = "<h3>Barbero Support</h3><br><h4>".$email_message."";
						$mail1->AddAddress($email_to);
						$mail1->SmtpClose();
						$mail1->Send();
						$mail2 = new PHPMailer(); // create a new object
						$mail2->IsSMTP(); // enable SMTP
						$mail2->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
						$mail2->SMTPAuth = true; // authentication enabled
						 // secure transfer enabled REQUIRED for Gmail
						$mail2->Host = $SMTPhost;
						$mail2->Port = 465; // or 587
						$mail2->IsHTML(true);
						$mail2->Username = $SMTPusername;
						$mail2->Password = $SMTPpassword;
						$mail2->SetFrom($SMTPfrom);
						$mail2->SMTPSecure = 'ssl';
						$mail2->Subject = "Barbero Support";
						$mail2->Body = "<h3>Barbero Support</h3><br><h4>Thank you for submitting your details</h4><h4>We will contact you shortly</h4>";
						$mail2->AddAddress($_POST["email"]);
						$mail2->SmtpClose();
						$mail2->Send();
header('Location: '.'BARBER.html');						

}
?>
 

