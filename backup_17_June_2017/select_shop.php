<!DOCTYPE html>
<?php
require_once('config/config.php');
?>
<html lang="en">
<head>
  <title>Rock Paper - Explore Salons</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php" style="background:rgba(0,0,0,0.1);color:white;padding-bottom:80px;border-radius:15px;"><img src = "images/logo.png" class = "img-responsive" style = "width:180px;height:auto;"/></a>
    </div>
    <div class="collapse navbar-collapse" style = "padding-top:25px;" id="myNavbar">
      <ul class="nav navbar-nav" style = "padding-left:20px;">
        <li class="active"><a href="index.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Home</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">About</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Projects</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log out</a></li>';
		}
		?>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Register Barber Shop</a></li>
      </ul>
    </div>
  </div>
</nav><br><br>
<div class = "container text-center" style = "background:rgba(0,0,0,0.7);border-radius:15px;">
	<div class = "row" style = "border-style:solid;border-width:4px;padding: 5px;margin: 5px;color:white;border-radius:15px;">
		<h4 style = "color:white;">&#10004;Select Location - <b><i>Choose Barbershop</i></b> - Choose services - Select Timeslot - Make Payment</h4>
	</div>
</div>
<br><br>
	<div class = "container visible-lg visible-md hidden-sm hidden-xs">
		<div class = "jumbotron" style = "background:rgba(0,0,0,0.7);">
			<h3 style = "color:white;">Barber shops available at your location are:</h3><br><hr>
			<div class = "container" style = "background:rgba(255,255,255,0.7);border-radius: 15px;">
				<div class = "row">
					<div class = "col-xs-4">
						<img src = "images/barber_shop_1.jpg" style = "margin:10px 10px 10px 10px;height:200px;width:auto;padding:1px;border:1px solid #021a40;background-color:white;border-radius: 15px;"></img>
					</div>
					<div class = "col-xs-8">
						<h3>Awesome Barber Shop, Kota</h3><br>
						<h4>Get awesome haircuts at awesome barber shop.</h4>
						<h4>First time is free! Book Now!</h4>
						<a href = "select_service.php" class = "btn btn-default">Get an appointment</a>
					</div>
			</div>
		</div>
		<hr>
		<div class = "container" style = "background:rgba(255,255,255,0.7);border-radius:15px;">
				<div class = "row">
					<div class = "col-xs-4">
						<img src = "images/background_2.jpg" style = "margin:10px 10px 10px 10px; height:200px;width:auto;padding:1px;border:1px solid #021a40;background-color:white;border-radius: 15px;"></img>
					</div>
					<div class = "col-xs-8">
						<h3>Special Barber Shop, Kota</h3><br>
						<h4>Special barber shop for your special needs.</h4>
						<a href = "select_service.php" class = "btn btn-default">Get an appointment</a>
					</div>
			</div>
		</div>
	</div>
	</div>
<div class = "container hidden-lg hidden-md visible-sm visible-xs">
		<div class = "jumbotron" style = "background:rgba(255,255,255,0.7);">
			<h3>Barber shops available at your location are:</h3><br><hr>
			<div class = "container" style = "background:rgba(255,255,255,0.7);border-radius: 15px;">
				
					
						<img src = "images/barber_shop_1.jpg" style = "margin-top:10px; height:auto;width:200px;border:1px solid #021a40;background-color:white;border-radius: 15px;"></img>
					
					
						<h3>Awesome Barber Shop, Kota</h3><br>
						<h4>Get awesome haircuts at awesome barber shop.</h4>
						<h4>First time is free! Book Now!</h4>
						<a href = "select_service.php" class = "btn btn-default" style="margin-bottom:10px;">Get an appointment</a>
					
			
		</div>
		<hr>
		<div class = "container" style = "background:rgba(255,255,255,0.7);border-radius:15px;">
				
						<img src = "images/background_2.jpg" style = "margin-top:10px; height:auto;width:200px;border:1px solid #021a40;background-color:white;border-radius: 15px;"></img>
				
						<h3>Special Barber Shop, Kota</h3><br>
						<h4>Special barber shop for your special needs.</h4>
						<a href = "select_service.php" class = "btn btn-default" style = "margin-bottom:10px;">Get an appointment</a>
			
		</div>
	</div>
	</div>
	
<br><br><br><br><br>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
  <div class = "row">
  <div class = "col-sm-4">
  </div>
  <div class = "col-sm-1">
  <center>
  <img src = "images/facebook_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <img src = "images/instagram_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <img src = "images/linkedin_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <img src = "images/google_plus_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/>
  </center>
  </div>
  <div class = "col-sm-4">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
	Copyright © 2017 Rock Paper - All Rights Reserved.
  </center>
  </div>
</footer>
</body>
</html>
				
			
			