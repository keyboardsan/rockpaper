<?php
	require_once('config/config.php');
	unset($_SESSION['user_id']);
	unset($_SESSION['firstname']);
	unset($_SESSION['lastname']);
	unset($_SESSION['email']);
	unset($_SESSION['mobile']);
	header('Location: '.'index.php');
?>