<!DOCTYPE html> <!--declaring type of document-->
<?php  /*opening a PHP block*/
require_once('config/config.php'); /*including the file where database credentals are stored*/
$mysqli = new mysqli($hostname, $username, $password, $dbname); /*opening a database connection*/
if ($mysqli->connect_error) {  /*checking for error in opening connection*/
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
?>

<html lang="en">
<head>
  <title>Rock Paper - Explore Salons</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style = "padding-top:25px;">
        <span class="icon-bar" style="color:black;"><b>&#9776;</b></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php" style="background:rgba(0,0,0,0.1);color:white;padding-bottom:80px;border-radius:15px;"><img src = "images/logo.png" class = "img-responsive" style = "width:180px;height:auto;"/></a>
    </div>
    <div class="collapse navbar-collapse" style = "padding-top:25px;" id="myNavbar">
      <ul class="nav navbar-nav" style = "padding-left:20px;">
        <li class="active"><a href="index.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Home</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">About</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Projects</a></li>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right" style = "padding-left:20px;">
	  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Logged in as '.$_SESSION['email'].'</a></li>';
		echo '<li><a href="logout.php" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Log out</a></li>';
		}
		?>
        <li><a href="#" style="color:white;background:rgba(0,0,0,0.6);border-style:solid;border-width:3px;">Register Barber Shop</a></li>
      </ul>
    </div>
  </div>
</nav><br><br>
<div class = "container text-center" style = "background:rgba(0,0,0,0.7);border-radius:15px;">
	<div class = "row" style = "border-style:solid;border-width:4px;padding: 5px;margin: 5px;color:white;border-radius:15px;">
		<h4 style = "color:white;"><b><i>Select Location</i></b> - Choose Barbershop - Choose services - Select Timeslot - Make Payment</h4>
	</div>
</div>
<br><br>
	<div class = "container">
		<div class = "jumbotron" style = "background:rgba(0,0,0,0.7);" id = "jumbotron_div">
			<div class = "container text-center" style = "width:400px;">
				<h3 style = "color:white;" id = "text_1">Select Location</h3>
					<form action = "select_shop.php" action = "POST">
						<select class = "form-control">
							  <?php
							    $sql="SELECT city_name,city_state from cities ORDER BY city_name ASC"; /*specifying the query to run*/
								$result=mysqli_query($mysqli,$sql); /*loading the query in $result variable*/
								while($row=mysqli_fetch_assoc($result)) { /*starting a while loop to traverse through the results returned by our query*/
								echo '<option value="'.$row["city_id"].'">'.$row["city_name"].'</option>'; /* echoing each row
								in <option> tag of <select> statement*/
								}
								?>
						</select><br>
						<input type = "submit" class = "btn btn-default" value = "Search">
					</form>
			</div>
		</div>
	</div>
	
<br><br><br><br><br><br><br><br><br><br>
<footer class="container-fluid text-center" style = "background:rgba(0,0,0,0.8);">
  <div class = "row">
  <div class = "col-sm-4">
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.facebook.com/rockpaperindia"><img src = "images/facebook_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.instagram.com/rockpaperindia/"><img src = "images/instagram_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://www.linkedin.com/rockpaperindia"><img src = "images/linkedin_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-1">
  <center>
  <a href = "https://plus.google.com/rockpaperindia"><img src = "images/google_plus_icon.png" style = "width:40px;height:40px;padding-bottom:5px;"/></a>
  </center>
  </div>
  <div class = "col-sm-4">
  </div>
  </div><br><br>
  <div class = "row" style = "color:white;">
  <center>
	Copyright © 2017 Rock Paper - All Rights Reserved.
  </center>
  </div>
</footer>
</body>
</html>