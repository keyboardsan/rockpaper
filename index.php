<!DOCTYPE html>
<?php
require_once('config/config.php');
$page = $_SERVER["REQUEST_URI"];
$_SESSION['page'] = $page;
$_SESSION["redirect_url"] = "http://".$_SERVER['SERVER_NAME'].$_SESSION['page'];
?>
<html >
<head>
<meta name="google-site-verification" content="sED9i4N0Na5SKpgzeCJgDAObf1FCvRYphHs6LYXkrbY" />
  <meta charset="UTF-8">
<title>Barbero - Barber Appointment Service</title>
         <LINK REL="SHORTCUT ICON"
       HREF="images/sssssshhh.png">
      <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Questrial">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=News+Cycle">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Amatic+SC">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Coming+Soon">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC">
               <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  
  <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>

      <link rel="stylesheet" href="css/css/style.css">
                <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
               <style>
                     .button {
    background-color: #c1292e; /*button color*/
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
}
                     .button2:hover {
    box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
}
               </style>

  
</head>

<body>
  <body>
  <header>
    <nav class="navbar navbar-default navbar-fixed-top navbar-inner">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand brand" href="index.php" style="background:;color:;padding-bottom:50px;border-radius:20px;"><img src = "images/BARBEROx.png" class = "img-responsive" style = "width:200px;height:100;"/></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#home">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="booking.php">Book Appointment</a></li>
            <li><a href="BARBER.html">For Barbers</a></li>
                            <li><a href="#">Download App</a></li>

                  <?php
		if(!isset($_SESSION['user_id'])) {
	    echo '<li><a href="login.php" style="color:black;background:;border-style:;border-width:1px;">Log in</a></li>';
	    echo '<li><a href="signup.php" style="color:black;background:;border-style:;border-width:3px;">Sign up</a></li>';
		}
		if(isset($_SESSION['user_id'])) {
	    echo '<li><a href="#" style="color:black;background:);border-style:;border-width:1px;">Logged in as '.$_SESSION['firstname'].'</a></li>';
		echo '<li><a href="logout.php" style="color:black;background:;border-style:;border-width:1px;">Log out</a></li>';
		}
		?>
            <li><a href="#contact">Contact</a></li>
          </ul>

   <!-- ---------------------------------------------------------------------------- --------------------------------------------------------->
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>
    <a name="home"></a>
    <div class="container-fluid">
      <div class="more-space row">
        <div class="col-md-12">
          <div class="text-center intro">
            <h1>Why wait for your turn at barber shop </h1>
            
			<h4 class="red">Book an appointment at the best barbershop near you</h4>
			<br><br> <h3> <a href="booking.php" style= margin: 15px;text-decoration:none;"" class="action-button shadow animate blue">Book Appointment</a> </h3> </br> </br>
          
		  </div>
        </div>
      </div>
    </div>
    </div>
  </header>

     <!--Start features-->
        <div class="features text-center">
            <div class="container features">
                <div class="box">
                    <i class="fa fa-home fa-fw" aria-hidden="true"></i>&nbsp;</a>
                    <h3 class="upper">Find the barbershop</h3>
                    <p>
                    Explore Shops And Their Barbers
                    </p>
                </div>
                <div class="box">
                   <i class="fa fa-calendar" style="font-size:36px"></i>
                    <h3 class="upper">Book an appointment</h3>
                    <p>
                    Pick A Time & Pre-Pay For Your Cut
                    </p>
                </div>
                <div class="box">
                    <i class="fa fa-scissors" style="font-size:36px"></i>
                    <h3 class="upper">Get Your Cut</h3>
                    <p>
                   Go get Your Haircut Without Waiting
                    </p>
                </div>
              
            </div>
        </div>
        <!--End features-->



  <!----ABOUT US---->

  <div class="about-us" id="startchange">
    <a name="about"></a>
    <div class="container-fluid">
      <div class="row text-center">
        <div class="descript col-xs-12 col-sm-12 col-md-12">
          <h1>ABOUT US</h1>
          <p class="about-us-p">
           We are bunch of people just enter into the startup world . Barbero is barber appointment service which provide booking at barbershop near you
		   from our app you can book your seat at your convenient date and time.</p>
            <a href="ourteam.html" class="team">Our Team</a>
        </div>
      </div>
    </div>
  </div>



  <!----SERVICES---->
  <div class="works">
    <a name="works"></a>
    <div class="container-fluid">

      <div class="row">
        <div class="text-center col-xs-12 col-sm-12 col-md-12">
          <h1>Our Service</h1>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4">
          <div class="thumbnail">
            <img src="images/service1.jpg" class="img-responsive" />
            <div class="caption">
              <h3>Barbershop Appointment Service</h3>
              <p>Book an appointment at barberhop</p>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-4 col-md-4">
          <div class="thumbnail">
            <img src="images/ladies.jpg" class="img-responsive" />
            <div class="caption">
              <h3>Ladies Parlour Appointment Service</h3>
              <p>Book an appointment at Ladies Parlour</p>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-4 col-md-4">
          <div class="thumbnail">
            <img src="images/service3.jpg" class="img-responsive" />
            <div class="caption">
              <h3>Home Service</h3>
              <p>Book the barber at your home</p>
            </div>
          </div>
        </div>

      </div>

    
          </div>
        </div>

      </div>


    </div>
  </div>

<div class="ajdownloadApp">
	<div class="ajContainer">
		<div class="ajRow ajtext-center">
			<div class="ajCol6 ajColM6 ajColS12">
				<div class="ajSpacing"></div>
				<h2 class="ajPageHeader">Download our App</h2>
				<h3 class="ajPageSubHeader">Book the finest barber in your city</h3>
				<a href="" target="blank" class="ajAppBtn ajPlay">
					<i class="fa fa-android fa-2x"></i>
					<small>Get it on</small>
					<span>Google Play</span>
				</a>
				<a href="" target="blank" class="ajAppBtn ajiStore">
					<i class="fa fa-apple fa-2x"></i>
					<small>Available on</small>
					<span>App Store</span>
				</a>
				<div class="ajSpacing"></div>
			</div>
			<div class="ajCol6 ajColM6 ajColS12 ajHidden-xs">
			</div>
		</div>
	</div>
</div>  <!-- download app -->




<div class="ajSpacing"></div>
			</div>
 
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/597dc4935dfc8255d623f9ad/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
 


  <div class="break1 container-fluid text-center">
    <h3>We Serve In these Cities!</h3>
   <p class="about-us-p">
           Delhi &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Gurgaon &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp  Noida</p>
    
  </div>
<div class="ajSpacing"></div>
			</div>
			<div class="ajCol6 ajColM6 ajColS12 ajHidden-xs">
			</div>
    <div class="break2 container-fluid text-center">
    <h1>Become a Barbero Barber!</h1>
	<br> <h1> <a href="BARBER.html" style= margin: 15px;text-decoration:none;"" class="action-button shadow animate blue">For Barbers</a> </h1> </br> 
     
  </div>
  
 <div class="ajSpacing"></div>
			</div>
  
<!--Start testimonial-->
      <!-- new testimonial -->
  
  <section class="testimonials">
<div class="overlay">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="testimonials-carousel">
							<ul>
								<li>
									<div class="testimonial">
										<div class="testimonial-img">
											<img src="http://demo.cantothemes.com/html/sept/photos/clent.jpg" alt="">
										</div>
										<blockquote>
											<p>Barbero is a game changer.</p>
											<footer>Pranay Tahiliani, <cite title="Source Title">Tax2win,Jaipur.</cite></footer>
										</blockquote>
									</div>
								</li>
								<li>
									<div class="testimonial">
										<div class="testimonial-img">
											<img src="http://demo.cantothemes.com/html/spt/photos/client2.jpg" alt="">
										</div>
										<blockquote>
											<p>It makes hair cutting very easy, all you have to book and get your haircut without wait.</p>
											<footer>Tarun, <cite title="Source Title">HCl,Noida.</cite></footer>
										</blockquote>
									</div>
								</li>
								<li>
									<div class="testimonial">
										<div class="testimonial-img">
											<img src="http://demo.cantothemes.com/htm/sept/photos/client3.jpg" alt="">
										</div>
										<blockquote>
											<p>Best service in town.</p>
											<footer>Akash, <cite title="Source Title">ICICI,New Delhi</cite></footer>
										</blockquote>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
        </div>
		</section>

 <!-- new testimonial -->
        <!--End testimonial -->
  
  
  <div class="contact text-center">
    <a name="contact"></a>
    <div class="container-fluid">

      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <h1>CONTACT</h3>
        </div>
      </div>
      
      <div class="row">
      
        <div class="col-xs-12 col-sm-12 col-md-12">
          <h2>Better yet, See us in person!</h2>
		 <h2> We love our customers, so feel free to visit during normal business hours.</h2>
          <p>Barbero</p>
		  <p>Kota, Rajasthan</p>
		  <p>info@barbero.in</p>
		  		  <p>+91 8094998542</p>
          <br/>
          <br/>
          <h3></h3>
        </div>
        
      </div>
      
     
       
        
      </div>
      
    
       
        
      </div>
      
      
      
    </div>
    
  </div>
 <!--start Contact-->
        <div class="contact text-center">
            <div class="overlay">
                <div class="container">
                    <h1 class="upper">Contact<span class="color">Us</span></h1>
                <p> Feel free to write us your queries
                    </p>
                    <form action="contact_send_email.php" method="POST">
                        <input type="text" placeholder="Your Name ..." name="name"/>
                        <input type="email" placeholder="Your Email" name="email"/>
                        <textarea placeholder="Your message ..." name="message"></textarea>
                      <center> <button class="button button2">Submit</button></center>
                        </form>
                </div>
            </div>
        </div>
        <!--End Contact-->
         
<div class="ajSpacing"></div>
			</div>
		 
             
<footer>
  <div class="container-fluid">
    
   <div class="row">
      
  
  </div><br><br><br><br><br>
        <div class = "row" style = "color:white;">
              <div class = "col-sm-1">
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.facebook.com/Barbero-433175553731182/"><img src = "images/facebook_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.instagram.com/barbero.in/"><img src = "images/instagram_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://www.linkedin.com/company-beta/13343377"><img src = "images/linkedin_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://plus.google.com/u/2/111476464313954727781"><img src = "images/google_plus_icon.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div>
  <div class = "col-sm-2">
  <center>
  <a href = "https://twitter.com/Barberoindia"><img src = "images/twitter.png" style = "width:30px;height:30px;padding-bottom:1px;"/></a>
  </center>
  </div><br><br><br>
  <center>
		Copyright © Barbero - All Rights Reserved.
  </center><br><br>
      </div>
      </div>
      </div>
    </div>
</footer>


  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>

    <script src="js/js/index.js"></script>

</body>
</html>
