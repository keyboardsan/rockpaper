<html lang="en">
<head>
  <title>Barbero</title>
  <link rel="shortcut icon" href="favicon.ico?v=2" type="image/x-icon" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="qrcode.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>
<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	require_once('config/config.php');
	require_once('phpqrcode/qrlib.php');
	require_once 'classes/phpmailer/mail.php';
	$mysqli = new mysqli($hostname, $username, $password, $dbname); /*opening a database connection*/
	if ($mysqli->connect_error) {  /*checking for error in opening connection*/
        die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
    }
    $time_from = $_SESSION["time_from"]->format("d M Y h:i:s a");
	$time_to = $_SESSION["time_to"]->format("d M Y h:i:s a");
	$validation = date("y-m-d h:i:sa");
	$validation = str_replace(" ","",$validation);
	$validation = str_replace("-","",$validation);
	$validation = str_replace(":","",$validation);
	$validation = str_replace("pm","",$validation);
	$validation = str_replace("am","",$validation);
	$sql = "INSERT INTO booking (bid,bsid,uid,booktime,finishtime,date,totalamount,servicetime,validation,totaltime) VALUES ('".$_SESSION["bofsid"]."','".$_SESSION["bsid"]."','".$_SESSION["user_id"]."','".$time_from."','".$time_to."','".$time_from."','".$_SESSION["total_price"]."','','".$validation."','".$_SESSION["total_time"]."')";
	if (mysqli_query($mysqli,$sql)) {
	echo '<div class = "container"><div class = "row"><div class = "col-sm-6">';
	echo '<h2>Barbero Appointment Booking Receipt</h2>';
	echo '<hr><div id = "qrcode" style = "float:right;"></div>';
	echo '<h4>Customer Name: '.$_SESSION["firstname"].' '.$_SESSION["lastname"].'</h4>';
	echo '<h4>Customer Email: '.$_SESSION["email"].'';
	echo '<h4>Customer Mobile: '.$_SESSION["mobile"].'<hr>';
	echo '<h3>Order details</h3>';
	$sql="SELECT bookid FROM booking WHERE validation = '".$validation."'"; /*specifying the query to run*/
	$result=mysqli_query($mysqli,$sql); /*loading the query in $result variable*/
	while($row = mysqli_fetch_assoc($result)) {
		$bookid = $row["bookid"];
	}
	echo '<h4>Shop: '.$_SESSION["shopname"].'</h4><h5>Address: '.$_SESSION["addressline1"].'</h5><h5>&nbsp;'.$_SESSION["addressline2"].'</h5><h5> Contact: '.$_SESSION["shopcontact"].'</h5>
		<h4>Barber: '.$_SESSION["barbername"].'</h4><h4>Services:';
		for($i=0;$i<$_SESSION["no_of_services"];$i++) {
			echo '<h5>'.$_SESSION["typearray"][$i].'</h5><h5>Rs. '.$_SESSION["pricearray"][$i].' | Duration '.$_SESSION["durationarray"][$i].' minutes</h5><hr>';
			$sql = "INSERT INTO bookingservice (bookid,sid) VALUES ('".$bookid."','".$_SESSION["idarray"][$i]."')";
			mysqli_query($mysqli,$sql);
		}
		echo '<h4><div style = "float:left;">Total price (Rs.): '.$_SESSION["total_price"].'</div></h4><br>';
		echo '<h4><div style = "float:left;">Duration (minutes): '.$_SESSION["total_time"].'</div></h4><br>';
		echo '<h4><div style = "float:left;">Start time: '.$_SESSION["time_from"]->format('d M Y h:i:s a').'</div></h4><br>';
		echo '<h4><div style = "float:left;">End time: '.$_SESSION["time_to"]->format('d M Y h:i:s a').'</div></h4><br><br><br><h5><i>A copy of the receipt has been sent to your registered email</i></h5>
<h5><i>Thank you for choosing barbero.in</i></h5><form>
    <input type="button" name="print" class = "form-control" value="Print" onClick="window.print()">
</form></div><div class = "col-sm-6"></div></div></div>';
						$mail1 = new PHPMailer(); // create a new object
						$mail1->IsSMTP(); // enable SMTP
						$mail1->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
						$mail1->SMTPAuth = true; // authentication enabled
						 // secure transfer enabled REQUIRED for Gmail
						$mail1->Host = $SMTPhost;
						$mail1->Port = 465; // or 587
						$mail1->IsHTML(true);
						$mail1->Username = $SMTPusername;
						$mail1->Password = $SMTPpassword;
						$mail1->SetFrom($SMTPfrom);
						$mail1->SMTPSecure = 'ssl';
						$mail1->Subject = "Booking Made | Barbero";
						$mail1->Body = "<p>You have successfully made a booking on barbero.in. To view your receipt visit the given link. Don't forget to bring the receipt in print or in your mobile phone when you go to avail the service. -></p><p><a href = '".$url."/view_receipt.php?id=".$validation."'>View Receipt</a></p>";
						$mail1->AddAddress($_SESSION["email"]);
						$mail1->SmtpClose();
						$mail1->Send();
	}else {
						echo "Error: " . $sql . "<br>" . mysqli_error($mysqli);
					}
?>

<script type="text/javascript">
var id = <?php echo $validation;?>;
var qrcode = new QRCode("qrcode", {
    text: "http://www.barbero.in/view_receipt.php?id="+id,
    width: 64,
    height: 64,
    colorDark : "#000000",
    colorLight : "#ffffff",
    correctLevel : QRCode.CorrectLevel.H
});
</script>
</body>
</html>